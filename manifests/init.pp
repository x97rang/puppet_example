# Class: php_ini

class php_ini($main_config = "php_ini/php_ini.erb") {

    notify { "### Main config is $main_config": }
    notify { "### Check php.ini for content": }
    file { '/etc/php5/apache2/php.ini':
      ensure  => file,
      backup  => true,
      content => template($main_config),
    }

    notify { "### Check config for restart apache2": }
    exec { "/bin/cp /root/restart_all_apaches.sh /root/restart_all_apaches2.sh":
           unless => "/usr/bin/test -f /root/restart_all_apaches.sh",
    }
    exec { "/root/restart_all_apaches2.sh restart":
	    subscribe => File["/etc/php5/apache2/php.ini"],
    	    refreshonly => true
    }
}

