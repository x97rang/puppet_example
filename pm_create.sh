#!/bin/bash
# j4ck, 11/04/2013

if [ -z $1 ] ; then 
	echo -------------------------------------------
	echo Скрипт для создания шаблона модулей Puppet
	echo -------------------------------------------
	echo
	echo Укажите имя модуля первым аргументом
	echo
	exit
fi

NAME=$1
#MDIR="/etc/puppetlabs/puppet/modules/"
MDIR="/opt/puppet/share/puppet/modules/"
LIST="manifests files templates lib tests spec"
DATE=`date +%d-%m-%Y_%H:%M:%S`

mkdir $MDIR/$NAME

for SUB in $LIST
do
	mkdir $MDIR/$NAME/$SUB
done

cat > $MDIR/$NAME/Modulefile <<EOF
name    '$NAME'
version '0.0.1'
author 'Evgeny Svirepov <evgeny.s@astrastudio.ru>'
summary ''
description ''
EOF

vim $MDIR/$NAME/Modulefile 

cat > $MDIR/$NAME/README <<EOF
$NAME

This is the $NAME module.
EOF

cat > $MDIR/$NAME/metadata.json <<EOF
{
  "source": [],
  "license": "GPLv3",
  "checksums": {
    "manifests/init.pp": ""
  },
  "dependencies": [],
  "types": [],
  "description": "",
  "summary": "",
  "name": "$NAME",
  "author": "Evgeny Svirepov <evgeny.s@astrastudio.ru>",
  "version": "0.0.1"
}
EOF

vim $MDIR/$NAME/metadata.json 


cat > $MDIR/$NAME/manifests/init.pp <<EOF
# Class: $NAME
# created $DATE by $0

class $NAME {



}
EOF

chown -R  pe-puppet.pe-puppet $MDIR/$NAME


